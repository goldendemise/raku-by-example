import React from "react"
import styled from 'styled-components'

export default class Footer extends React.Component {

  render() {
    return (
      <FooterContainer>
          <p>
          <a href="https://docs.perl6.org/">Docs</a> |
          <a href="https://gitlab.com/uzluisf/raku-by-example">Source</a> |
           by&nbsp;
          <a href="https://uzluisf.gitlab.io/">Luis F. Uceta</a>
        </p>
      </FooterContainer>
    )
  }
}

const FooterContainer = styled.div`
  p > a {
    color: grey;
  }
`;
